# Implement ArgoCD using HELM

## Prerequisites
1. kubernetes cluster
2. helm chart

### create argocd namespace :
```
kubectl create namespace argocd
```

### Add a Helm repository named "argocd"
```
helm repo add argo https://argoproj.github.io/argo-helm
```


### updates the local cache of available Helm charts from all added repositories
```
helm repo update
```


### upgrade a Helm release named "argocd"
```
helm upgrade --install argocd argo/argo-cd --namespace argocd
```


### Retrieve the value of the "argocd-initial-admin-password" key from the argocd secret in the "argocd" namespace.
```
kubectl get secret --namespace argocd argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```


### Set up a port forwarding between your local machine's port 8080.
```
kubectl --namespace argocd port-forward svc/argo-cd-argocd-server 8080:443
```

### connect to jenkins server in adress: http//localhost:8080 && login with the admin user and password from previews step.




Clean UP:
```
helm uninstall argocd --namespace argocd
```
